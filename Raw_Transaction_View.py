import streamlit as st
import pandas as pd
import numpy as np
from io import StringIO

def clean(file):
    valid_lines = []
    blank_line_count = 0
    for line in file:
            decoded_line = line.decode('utf-8').strip()
            if decoded_line.strip():
                 valid_lines.append(decoded_line.strip())
            else:
                 blank_line_count += 1
            if blank_line_count >=2:
                 break
    
    df = pd.read_csv(StringIO("\n".join(valid_lines)))
    df['action_simple'] = np.where(
    df['Action'].str.contains(r'\bYOU BOUGHT\b', regex=True), 'buy',
    np.where(df['Action'].str.contains(r'\bYOU SOLD\b', regex=True), 'sell', None))
    if "Account" not in df.columns:
        df = df.drop(["Type", "Commission ($)", "Fees ($)", "Accrued Interest ($)", "Settlement Date", "Action"], axis=1)
    else: 
        df = df.drop(["Account", "Type", "Commission ($)", "Fees ($)", "Accrued Interest ($)", "Settlement Date", "Action"], axis=1)
    return df
                 


def display():
    uploaded_file = st.file_uploader("Choose a CSV file", type="csv")
    
    if uploaded_file is not None:
        # uploaded_file = pd.read_csv(uploaded_file)
        # uploaded_view(st.dataframe(uploaded_file))
        cleaned_df = clean(uploaded_file)

        filter_option = st.radio(
             "Choose what to display:",
             ("Show All", "Only Show Sell"))
        
        if filter_option == "Only Show Sell":
            cleaned_df = cleaned_df[cleaned_df["action_simple"] == "sell"]
            cleaned_df = cleaned_df.reset_index(drop=True)
            st.dataframe(cleaned_df)
        else:
            cleaned_df = cleaned_df.sort_values(by=['Symbol', 'Run Date'])
            cleaned_df = cleaned_df.reset_index(drop=True)
            st.dataframe(cleaned_df)
           

if __name__ == "__main__":
    pass