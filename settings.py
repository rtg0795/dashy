import sys
import os

project_path = os.path.split(os.path.realpath(__file__))[0]
holdings_path = os.path.expanduser('~/database/holdings.csv')
stock_data_path = os.path.expanduser('~/stock_data/')
settled_path = os.path.expanduser('~/database/settled.csv')
sys.path.append(project_path)

if __name__ == "__main__":
    pass

