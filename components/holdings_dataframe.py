import streamlit as st
import pandas as pd
import os
from st_aggrid import AgGrid, GridOptionsBuilder

def get_latest_price(symbol, stock_data_path):
    file_path = os.path.join(stock_data_path, f"{symbol}.csv")
    if os.path.exists(file_path):
        try:
            df = pd.read_csv(file_path, header=0)
            latest_price = df.iloc[-1]['Close']
            return round(latest_price, 2)
        except Exception as e:
            st.error(f"Error reading data for {symbol}: {str(e)}")
    else:
        st.error(f"Data file not found for {symbol}")
    return None

def get_holdings_indicators(df_specific, latest_price):
    total_quantity = round(df_specific['quantity'].sum(), 4)
    average_price = round((df_specific['quantity'] * df_specific['price']).sum() / total_quantity, 2)
    total_holdings = round(total_quantity * average_price, 2)
    estimated_profit = round((latest_price * total_quantity) - total_holdings, 2)
    prices_bought = df_specific['price'].tolist()
    prices_bought_format = ", ".join([f"${price:.2f}" for price in sorted(prices_bought)])
    
    indicator_df = pd.DataFrame({
        'Indicator': ['Total Quantity', 'Average Price $', 'Total Holdings $', 'Estimated Profit $', 'Prices bought $'],
        'Value': [total_quantity, average_price, total_holdings, estimated_profit, prices_bought_format]
    })
    
    return indicator_df

def display(holdings_path, stock_data_path):
    holdings_df = pd.read_csv(holdings_path, header=0)
    unique_symbols = holdings_df['symbol'].unique()
    
    for symbol in unique_symbols:
        st.header(f"**{symbol}**")
        latest_price = None
        
        # Use a spinner while fetching the latest price
        with st.spinner(f"Fetching Latest Price for {symbol}..."):
            latest_price = get_latest_price(symbol, stock_data_path)
        
        if latest_price is not None:
            st.markdown(f"*Latest Price: ${latest_price}*")
            
            df_specific = holdings_df[holdings_df['symbol'] == symbol]
            df_specific['date'] = pd.to_datetime(df_specific['date']).dt.date  # Extract date part
            
            # Explicitly format the date column to display only the date
            df_specific['date'] = df_specific['date'].apply(lambda x: x.strftime('%Y-%m-%d'))
            
            df_specific = df_specific.sort_values(by='date', ascending=True)
            
            # Calculate the 'percentage change' column
            df_specific['percentage change'] = round((latest_price - df_specific['price']) / df_specific['price'] * 100, 2)
            
            # Display indicators
            st.table(get_holdings_indicators(df_specific, latest_price))
            
            # Create a GridOptionsBuilder for df_specific
            gb = GridOptionsBuilder.from_dataframe(df_specific)
            
            # Set column autoWidth for better column size management
            gb.configure_default_column(groupable=True, value=True, enableRowGroup=True, aggFunc='sum', editable=True, autoWidth=True)
            
            # Build the GridOptions for df_specific
            grid_options = gb.build()
            
            # Display df_specific using AgGrid with the configured options
            AgGrid(df_specific, gridOptions=grid_options, fit_columns_on_grid_load=True, allow_unsafe_jscode=True)
            
            st.divider()

if __name__ == "__main__":
    pass
