import streamlit as st
import pandas as pd
import settings
from st_aggrid import AgGrid, GridOptionsBuilder
from st_aggrid.shared import JsCode


def load_and_process_data(filepath):
    data = pd.read_csv(filepath, index_col=None)
    data['amount'] = data['price'] * data['quantity']
    data['date'] = pd.to_datetime(data['date']).dt.date

    results = []
    current_id = 0
    total_buy_amount = 0.0

    for index, row in data.iterrows():
        if row['type'] == 'buy':
            total_buy_amount += row['amount']
        elif row['type'] == 'sell':
            profit_loss = row['amount'] - total_buy_amount
            results.append({
                'id': current_id,
                'symbol': row['symbol'],
                'sell_date': row['date'],
                'profit_loss': profit_loss
            })
            current_id += 1
            total_buy_amount = 0.0  # Reset for the next 'brick'

    result_df = pd.DataFrame(results)
    result_df = result_df.sort_values(by='sell_date', ascending=False).reset_index(drop=True)
    return result_df

def display_monthly_profits_and_transactions(transactions):
    transactions['year_month'] = transactions['sell_date'].apply(lambda x: x.strftime('%Y-%m'))
    monthly_profits = transactions.groupby('year_month')['profit_loss'].sum().sort_index(ascending=False)

    for year_month, profit in monthly_profits.items():
        st.subheader(f"Month: {year_month}")
        st.metric(label="Total Profit/Loss", value=f"${profit:.2f}")

        # Filter the transactions for the specific month
        monthly_transactions = transactions[transactions['year_month'] == year_month]

        # Display only the date part in the 'sell_date' column
        monthly_transactions['sell_date'] = monthly_transactions['sell_date'].apply(lambda x: x.strftime('%Y-%m-%d'))

        # Drop the 'year_month' column
        monthly_transactions = monthly_transactions.drop(columns=['year_month'])

        # Create a GridOptionsBuilder
        gb = GridOptionsBuilder.from_dataframe(monthly_transactions)

        # Set column autoWidth for better column size management
        gb.configure_default_column(groupable=True, value=True, enableRowGroup=True, aggFunc='sum', editable=True, autoWidth=True)

        # Use JsCode for styling
        cell_style_jscode = JsCode("""
        function(params) {
            if (params.value > 0) {
                return {
                    'color': 'white',
                    'backgroundColor': 'green'
                }
            } else {
                return {
                    'color': 'white',
                    'backgroundColor': 'red'
                }
            }
        };
        """)

        # Apply the JsCode function to the 'profit_loss' column
        gb.configure_column("profit_loss", cellStyle=cell_style_jscode)

        # Build the GridOptions
        gridOptions = gb.build()

        # Display the DataFrame using AgGrid with the configured options
        AgGrid(monthly_transactions, gridOptions=gridOptions, fit_columns_on_grid_load=True, allow_unsafe_jscode=True)

def display_app():
    st.title("Monthly Profit/Loss Transactions")

    file_path = settings.settled_path

    # Create a spinner while data processing is happening
    with st.spinner("Calculating..."):
        transactions = load_and_process_data(file_path)

    # Display the transactions DataFrame
    st.dataframe(transactions[['id', 'symbol', 'sell_date', 'profit_loss']])

    # Initialize a placeholder for AgGrid
    ag_grid_placeholder = st.empty()

    # Display the loading message within the placeholder
    with ag_grid_placeholder:
        st.write("AgGrid Loading...")  # Add a loading message

    # Update the AgGrid with real data once it's ready
    transactions_df = transactions[['id', 'symbol', 'sell_date', 'profit_loss']]
    # Format 'profit_loss' column to two decimal places
    transactions_df['profit_loss'] = transactions_df['profit_loss'].apply(lambda x: round(x, 2))

    grid_options = GridOptionsBuilder.from_dataframe(transactions_df).build()

    # Replace the AgGrid placeholder with the actual AgGrid
    with ag_grid_placeholder:
        ag_grid = AgGrid(transactions_df, gridOptions=grid_options, fit_columns_on_grid_load=True, allow_unsafe_jscode=True)
        st.empty()  # Remove the loading message

    # Display monthly profits and transactions
    display_monthly_profits_and_transactions(transactions)

if __name__ == "__main__":
    pass
