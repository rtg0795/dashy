#!/usr/bin/env bash

# username@remote_host
remote_user_host=$1

base=$(echo $HOME)

rm -rf "${base}/database"
mkdir "${base}/database"
scp "${remote_user_host}:/root/database/*" "${base}/database/"

rm -rf "${base}/stock_data"
mkdir "${base}/stock_data"
scp "${remote_user_host}:/root/stock_data/*" "${base}/stock_data/"

echo "**Copying Done**"


