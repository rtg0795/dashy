import streamlit as st
import pandas as pd
import settings

def total_amount_desc(csv_path):
    # Read the CSV file from a specified path
    df = pd.read_csv(csv_path)

    # Calculate the total amount for each transaction
    df['total_amount'] = df['price'] * df['quantity']

    # Group by stock symbol and sum quantities and total amounts
    grouped = df.groupby('symbol').agg(total_quantity=('quantity', 'sum'), 
                                       total_amount=('total_amount', 'sum')).reset_index()

    # Sort the dataframe in descending order of total_amount
    df_grouped_sorted = grouped.sort_values('total_amount', ascending=False)

    return df_grouped_sorted

def display():
    st.title('Stock Holdings Analysis')

    # Read CSV file directly from the specified path in settings
    csv_path = settings.holdings_path
    df_grouped_sorted = total_amount_desc(csv_path)

    # Calculate the total sum of total_amount for all data
    total_sum = df_grouped_sorted['total_amount'].sum()
    st.metric(label="Total Amount", value=f"${total_sum:,.2f}")

    # Text input for tags
    tag_input_raw = st.text_input('Enter tags (separated by commas)')
    tag_input = [tag.strip() for tag in tag_input_raw.split(',') if tag.strip()]

    # Filter rows where 'symbol' column matches any of the tags
    if tag_input:
        filtered_df = df_grouped_sorted[df_grouped_sorted['symbol'].isin(tag_input)]
        if not filtered_df.empty:
            # Calculate and display the sum of total_amount for the filtered data
            filtered_sum = filtered_df['total_amount'].sum()
            st.metric(label="Filtered Total Amount", value=f"${filtered_sum:,.2f}")
            st.write(filtered_df)
        else:
            st.write("No matching rows found for the entered tags.")
    else:
        # Display the entire dataframe by default
        st.write(df_grouped_sorted)

if __name__ == "__main__":
    pass
