import streamlit as st
import pandas as pd
import os
import time
import settings
import Portfolio_Insights
import Holdings_Update
import Scorecard
import Holdings_Views
import Raw_Transaction_View

def main():
  st.set_page_config(
    page_title="Portfolio Insights",
    page_icon="🧊",
    layout="wide",
    initial_sidebar_state="expanded",
  )
  selected_page = st.sidebar.selectbox(
    "Select a page",
    [
      "Portfolio Insights",
      "Holdings Update",
      "Scorecard",
      "Holdings Views",
      "Raw Transaction View"
    ]
  )

  if selected_page == "Portfolio Insights":
    Portfolio_Insights.display()
  elif selected_page == "Holdings Update":
    Holdings_Update.display()
  elif selected_page == "Scorecard":
    Scorecard.display_app()
  elif selected_page == "Holdings Views":
    Holdings_Views.display()
  elif selected_page == "Raw Transaction View":
    Raw_Transaction_View.display()
    
if __name__ == "__main__":
  main()



