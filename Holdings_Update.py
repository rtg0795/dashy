import streamlit as st
import pandas as pd
import settings

def display():
  holdings_csv = pd.read_csv(settings.holdings_path, index_col=None)
  settled_csv = pd.read_csv(settings.settled_path, index_col=None)

  op_type = st.radio('Choose Operation', ('Add Holdings', 'Settle Holdings'), horizontal=True)

  if op_type == 'Add Holdings':
    st.title('Add Holdings')

    st.table(holdings_csv.sort_values(by='symbol', ascneding=True))

    col1, col2, col3, col4, col5 = st.columns(5)
    symbol = col1.text_input(label='symbol')
    quantity = col2.number_input(label='quantity')
    price = col3.number_input(label='price')
    date = col4.date_input(label='date')

    # `Save` button operations
    def save():
      if not symbol or not quantity or not price:
        pass
      else:
        df_copy = holdings_csv.copy(deep=True)
        df_copy.loc[len(df_copy.index)] = [symbol, price, quantity, date]
        df_copy.to_csv(settings.holdings_path, index=False)

    save_button = col5.button(label='Save', on_click=save)
  if op_type == 'Settle Holdings':
    st.title('Settle Holdings')

    with st.expander('Settled Transactions Log'):
      st.table(settled_csv)

    st.table(holdings_csv)
    filter_symbol = st.text_input(label='Enter symbol to settle')
    filtered_df = holdings_csv.loc[holdings_csv['symbol'] == filter_symbol]

    if len(filtered_df) != 0:
      st.write(f"Found rows for symbol {filter_symbol} with total quantity {filtered_df['quantity'].sum()}")
      st.table(filtered_df)
      col1, col2, col3 = st.columns(3)
      sold_price = col1.number_input(label='sold price')
      sold_date = col2.date_input(label='sold date')


      def settle():
        settled_df = pd.DataFrame(columns=['symbol', 'price', 'quantity', 'date', 'type'])

        for index, row in filtered_df.iterrows():
          settled_df.loc[len(settled_df)] = [row['symbol'], row['price'], row['quantity'], row['date'], 'buy']
        settled_df.loc[len(settled_df)] = [filter_symbol, sold_price, settled_df['quantity'].sum(), sold_date, 'sell'] 

        indexes_to_remove = filtered_df.index.tolist()
        holdings_csv.drop(indexes_to_remove, inplace=True)
        holdings_csv.to_csv(settings.holdings_path, index=False)

        for index, row in settled_df.iterrows():
          settled_csv.loc[len(settled_csv)] = [row['symbol'], row['quantity'], row['price'], row['date'], row['type']]
        settled_csv.to_csv(settings.settled_path, index=False)
      
      settle_button = col3.button(label='Settle', on_click=settle) 

  st.divider()

if __name__ == "__main__":
  pass
