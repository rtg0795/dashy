import streamlit as st
import pandas as pd
import os
import time
import settings
from components import holdings_dataframe

def display():           
    st.title("Portfolio Insights")    
    holdings_dataframe.display(holdings_path=settings.holdings_path, stock_data_path=settings.stock_data_path)

if __name__ == "__main__":
    pass




